ASSUME CS:CODE, DS:DATA
DATA SEGMENT
	msg db "hello world$"
DATA ENDS

CODE SEGMENT
START:	MOV AX, DATA
	MOV DS, AX

	LEA DX, msg
	MOV AH, 09H ;these 2 lines for calling 
	INT 21H	    ;print function
	
	MOV AH, 4CH
	INT 21H
CODE ENDS
END START

